package com.merchant.customcollections

import com.merchant.customcollections.models.Collection

/**
 * Created on 20/01/19.
 */
interface MainView {
    fun showCollectionsDetailView(collection: Collection)
}