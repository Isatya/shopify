package com.merchant.customcollections.views

import com.merchant.customcollections.models.Product

/**
 * Created on 20/01/19.
 */
interface CollectionDetailView {
    fun isAlive(): Boolean
    fun showLoader()
    fun hideLoader()
    fun showProducts(products: List<Product>)
}