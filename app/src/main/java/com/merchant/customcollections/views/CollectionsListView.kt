package com.merchant.customcollections.views

import com.merchant.customcollections.models.Collection

/**
 * Created on 20/01/19.
 */
interface CollectionsListView {
    fun isAlive(): Boolean
    fun showLoader()
    fun hideLoader()
    fun showCollections(collections: List<Collection>)
}