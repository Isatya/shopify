package com.merchant.customcollections.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.merchant.customcollections.MainView
import com.merchant.customcollections.R
import com.merchant.customcollections.adapters.CollectionsAdapter
import com.merchant.customcollections.api.CollectionsService
import com.merchant.customcollections.api.ServiceHelper
import com.merchant.customcollections.models.Collection
import com.merchant.customcollections.presenters.CollectionListPresenter
import com.merchant.customcollections.utils.ImageHelper
import com.merchant.customcollections.utils.RecyclerViewItemClickListener
import kotlinx.android.synthetic.main.fragment_collections_list.loader as loaderV
import kotlinx.android.synthetic.main.fragment_collections_list.recycler_view as collectionsListRv

/**
 * Created on 19/01/19.
 */
class CollectionsListFragment : Fragment(), CollectionsListView {

    private lateinit var presenter: CollectionListPresenter
    private lateinit var collectionsAdapter: CollectionsAdapter

    companion object {
        val TAG = "CollectionsListFragment"
        fun newInstance(): CollectionsListFragment {
            return CollectionsListFragment()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = CollectionListPresenter(
            this,
            ServiceHelper.create(CollectionsService::class.java)
        )

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_collections_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        collectionsAdapter = CollectionsAdapter(
            object : RecyclerViewItemClickListener<Collection> {
                override fun onClickItem(position: Int, collection: Collection) {
                    (activity as MainView).showCollectionsDetailView(collection)
                }
            },
            ImageHelper.getInstance()
        )
        collectionsListRv.layoutManager = LinearLayoutManager(context)
        collectionsListRv.adapter = collectionsAdapter
        collectionsListRv.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        presenter.getCollections()
    }

    override fun isAlive(): Boolean {
        return activity != null && isAdded
    }

    override fun showLoader() {
        loaderV.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        loaderV.visibility = View.GONE
    }

    override fun showCollections(collections: List<Collection>) {
        collectionsAdapter.setCollections(collections.toMutableList())
    }

}