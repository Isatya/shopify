package com.merchant.customcollections.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.merchant.customcollections.R
import com.merchant.customcollections.adapters.ProductsAdapter
import com.merchant.customcollections.api.CollectionsService
import com.merchant.customcollections.api.ServiceHelper
import com.merchant.customcollections.models.Collection
import com.merchant.customcollections.models.Product
import com.merchant.customcollections.presenters.CollectDetailPresenter
import com.merchant.customcollections.utils.ImageHelper
import com.merchant.customcollections.utils.inflate
import kotlinx.android.synthetic.main.fragment_collection_detail.back as backBtn
import kotlinx.android.synthetic.main.fragment_collection_detail.collection_body as collectionBodyIv
import kotlinx.android.synthetic.main.fragment_collection_detail.collection_image as collectionImageIv
import kotlinx.android.synthetic.main.fragment_collection_detail.loader as loaderV
import kotlinx.android.synthetic.main.fragment_collection_detail.recycler_view as productsRv
import kotlinx.android.synthetic.main.fragment_collection_detail.title as titleTv

/**
 * Created on 20/01/19.
 */
class CollectionDetailFragment : Fragment(), CollectionDetailView {

    private lateinit var presenter: CollectDetailPresenter
    private lateinit var productsAdapter: ProductsAdapter
    private lateinit var collection: Collection

    companion object {
        val TAG = "CollectionDetailFragment"

        fun newInstance(collection: Collection): CollectionDetailFragment {
            val bundle = Bundle()
            bundle.putParcelable("collection", collection)
            val fragment = CollectionDetailFragment()
            fragment.arguments = bundle
            return fragment
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        collection = arguments!!.getParcelable("collection")
        presenter = CollectDetailPresenter(
            this,
            collection,
            ServiceHelper.create(CollectionsService::class.java)
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container!!.inflate(R.layout.fragment_collection_detail, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleTv.text = collection.title
        collectionBodyIv.text = collection.body_html
        val imageHelper = ImageHelper.getInstance()
        imageHelper.loadImage(collectionImageIv, collection.image.src)
        backBtn.setOnClickListener {
            activity?.onBackPressed()
        }
        productsAdapter = ProductsAdapter(collection, imageHelper)
        productsRv.isNestedScrollingEnabled = false
        productsRv.layoutManager = LinearLayoutManager(context)
        productsRv.adapter = productsAdapter
        presenter.getCollects()
    }

    override fun isAlive(): Boolean {
        return activity != null && isAdded
    }

    override fun showProducts(products: List<Product>) {
        productsAdapter.products = products.toMutableList()
    }

    override fun showLoader() {
        loaderV.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        loaderV.visibility = View.GONE
    }

}