package com.merchant.customcollections.api

import com.merchant.customcollections.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created on 20/01/19.
 */
class ServiceHelper {

    companion object {
        //TODO move to config
        private const val BASE_URL = "https://shopicruit.myshopify.com/"

        private lateinit var instance: Retrofit

        fun init() {
            val loggingInterCeptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                loggingInterCeptor.level = HttpLoggingInterceptor.Level.BODY
            }
            val httpCLient = OkHttpClient.Builder()
            httpCLient.addInterceptor(loggingInterCeptor)

            instance = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpCLient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        fun <T> create(service: Class<T>): T {
            return instance.create(service)
        }

    }

}