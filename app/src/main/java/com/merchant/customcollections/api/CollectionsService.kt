package com.merchant.customcollections.api

import com.merchant.customcollections.models.GetCollectionsResponse
import com.merchant.customcollections.models.GetCollectsResponse
import com.merchant.customcollections.models.GetProductsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created on 19/01/19.
 */
interface CollectionsService {

    @GET("admin/custom_collections.json")
    fun getCustomCollectionsList(
        @Query("page") page: Int,
        @Query("access_token") accessToken: String
    ): Call<GetCollectionsResponse>

    @GET("admin/collects.json")
    fun getCollects(
        @Query("collection_id") collectionId: Long,
        @Query("page") page: Int,
        @Query("access_token") accessToken: String
    ): Call<GetCollectsResponse>

    @GET("admin/products.json")
    fun getProducts(
        @Query("ids") productIds: String,
        @Query("page") page: Int,
        @Query("access_token") accessToken: String
    ): Call<GetProductsResponse>

}