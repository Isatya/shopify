package com.merchant.customcollections

import android.app.Application
import com.merchant.customcollections.api.ServiceHelper
import com.merchant.customcollections.utils.ImageHelper

/**
 * Created on 20/01/19.
 */
class MerchantApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        ServiceHelper.init()
        ImageHelper.init(this)
    }

}