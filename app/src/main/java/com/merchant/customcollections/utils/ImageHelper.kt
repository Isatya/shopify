package com.merchant.customcollections.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions

/**
 * Created on 20/01/19.
 */
class ImageHelper(context: Context) {

    private val requestManager: RequestManager

    init {
        requestManager = Glide.with(context)
    }

    companion object {

        private lateinit var instance: ImageHelper

        fun init(context: Context) {
            instance = ImageHelper(context)
        }

        fun getInstance(): ImageHelper {
            return instance
        }

    }

    fun loadImage(imageView: ImageView, url: String) {
        requestManager.load(url)
            .into(imageView)
    }

    fun loadCircularImage(imageView: ImageView, url: String) {
        requestManager.load(url)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView)
    }

}