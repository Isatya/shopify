package com.merchant.customcollections.utils

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

/**
 * Created on 20/01/19.
 */
class ActivityHelper {

    companion object {

        fun replace(@IdRes containerId: Int, fragmentManager: FragmentManager, fragment: Fragment, tag: String) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(containerId, fragment, tag)
            fragmentTransaction.commit()
        }

        fun add(@IdRes containerId: Int, fragmentManager: FragmentManager, fragment: Fragment, tag: String) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(containerId, fragment, tag)
            fragmentTransaction.addToBackStack(tag)

            val displayedFragment = fragmentManager.findFragmentById(containerId)

            if (displayedFragment != null) {
                fragmentTransaction.hide(displayedFragment)
            }

            fragmentTransaction.commit()
        }

    }

}