package com.merchant.customcollections.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

/**
 * Created on 20/01/19.
 */
fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean): View {
    val li = LayoutInflater.from(context)
    return li.inflate(layoutRes, this, attachToRoot)
}