package com.merchant.customcollections.utils

/**
 * Created on 20/01/19.
 */
interface RecyclerViewItemClickListener<E> {
    fun onClickItem(position: Int, element: E)
}