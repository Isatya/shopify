package com.merchant.customcollections

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.merchant.customcollections.models.Collection
import com.merchant.customcollections.utils.ActivityHelper
import com.merchant.customcollections.views.CollectionDetailFragment
import com.merchant.customcollections.views.CollectionsListFragment

class MainActivity : AppCompatActivity(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showCollectionsListView()
    }

    private fun showCollectionsListView() {
        ActivityHelper.replace(
            R.id.container,
            supportFragmentManager,
            CollectionsListFragment.newInstance(),
            CollectionsListFragment.TAG
        )
    }

    override fun showCollectionsDetailView(collection: Collection) {
        ActivityHelper.add(
            R.id.container,
            supportFragmentManager,
            CollectionDetailFragment.newInstance(collection),
            CollectionDetailFragment.TAG
        )
    }

}
