package com.merchant.customcollections.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.merchant.customcollections.R
import com.merchant.customcollections.models.Collection
import com.merchant.customcollections.models.Product
import com.merchant.customcollections.utils.ImageHelper
import com.merchant.customcollections.utils.inflate

/**
 * Created on 20/01/19.
 */
class ProductsAdapter(
    private val collection: Collection,
    private val imageHelper: ImageHelper
) : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    var products = mutableListOf<Product>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(parent.inflate(R.layout.item_product, false), collection, imageHelper)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onBindView(products[position])

    override fun getItemCount() = products.size

    class ViewHolder(
        itemView: View,
        private val collection: Collection,
        private val imageHelper: ImageHelper
    ) : RecyclerView.ViewHolder(itemView) {

        private val productImageIv: ImageView = itemView.findViewById(R.id.image)
        private val productTitleTv: TextView = itemView.findViewById(R.id.title)
        private val productTagsTv: TextView = itemView.findViewById(R.id.tags)
        private val productBodyTv: TextView = itemView.findViewById(R.id.body)
        private val collectionIv: ImageView = itemView.findViewById(R.id.collection_image)
        private val collectionTitleTv: TextView = itemView.findViewById(R.id.collection_title)

        fun onBindView(product: Product) {
            productTitleTv.text = product.title
            imageHelper.loadImage(productImageIv, product.image.src)
            productTagsTv.text = product.getTagsString()
            productBodyTv.text = product.bodyHtml
            imageHelper.loadCircularImage(collectionIv, collection.image.src)
            collectionTitleTv.text = collection.title
        }

    }

}