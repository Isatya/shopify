package com.merchant.customcollections.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.merchant.customcollections.R
import com.merchant.customcollections.models.Collection
import com.merchant.customcollections.utils.ImageHelper
import com.merchant.customcollections.utils.RecyclerViewItemClickListener
import com.merchant.customcollections.utils.inflate

/**
 * Created on 19/01/19.
 */
class CollectionsAdapter(
    val recyclerViewItemClickListener: RecyclerViewItemClickListener<Collection>,
    val imageHelper: ImageHelper
) : RecyclerView.Adapter<CollectionsAdapter.ViewHolder>() {

    private var collections = mutableListOf<Collection>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.item_collection, false)
        return ViewHolder(view, recyclerViewItemClickListener, imageHelper)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBindView(collections[position])
    }

    override fun getItemCount(): Int {
        return collections.size
    }

    fun setCollections(collections: MutableList<Collection>) {
        this.collections = collections
        notifyDataSetChanged()
    }

    class ViewHolder(
        itemView: View,
        val recyclerViewItemClickListener: RecyclerViewItemClickListener<Collection>,
        val imageHelper: ImageHelper
    ) : RecyclerView.ViewHolder(itemView) {
        val collectionIv: ImageView
        val collectionTitleTv: TextView
        val collectionBodyTv: TextView

        private lateinit var collection: Collection

        init {
            collectionIv = itemView.findViewById(R.id.image)
            collectionTitleTv = itemView.findViewById(R.id.title)
            collectionBodyTv = itemView.findViewById(R.id.body)
            itemView.findViewById<View>(R.id.parent).setOnClickListener {
                recyclerViewItemClickListener.onClickItem(adapterPosition, collection)
            }
        }

        fun onBindView(collection: Collection) {
            this.collection = collection
            imageHelper.loadImage(collectionIv, collection.image.src)
            collectionTitleTv.text = collection.title
            collectionBodyTv.text = collection.body_html
        }

    }

}