package com.merchant.customcollections.models

import com.google.gson.annotations.SerializedName

/**
 * Created on 20/01/19.
 */
data class Collect(
    @SerializedName("id")
    val id: Long,
    @SerializedName("collection_id")
    val collectionId: Long,
    @SerializedName("product_id")
    val productId: Long
)