package com.merchant.customcollections.models

import com.google.gson.annotations.SerializedName
import com.merchant.customcollections.models.Collection

/**
 * Created on 20/01/19.
 */
data class GetCollectionsResponse(
    @SerializedName("custom_collections")
    val collections: List<Collection>
)