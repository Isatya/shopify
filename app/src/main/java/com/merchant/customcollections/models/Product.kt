package com.merchant.customcollections.models

import com.google.gson.annotations.SerializedName

/**
 * Created on 20/01/19.
 */
data class Product(
    @SerializedName("id")
    val id: Long,
    @SerializedName("title")
    val title: String,
    @SerializedName("body_html")
    val bodyHtml: String,
    @SerializedName("image")
    val image: Image,
    @SerializedName("variants")
    val variants: List<Variant>,
    @SerializedName("tags")
    val tags: String
) {
    private lateinit var tagsString: String

    fun getTagsString(): String {
        if (!::tagsString.isInitialized) {
            var inventorySize = 0
            for (variant in variants) {
                inventorySize += variant.inventoryQuantity
            }
            tagsString = tags + " (" + inventorySize + ")"
        }
        return tagsString
    }

}