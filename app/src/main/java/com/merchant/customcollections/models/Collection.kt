package com.merchant.customcollections.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created on 19/01/19.
 */
data class Collection(
    @SerializedName("id")
    val id: Long,
    @SerializedName("handle")
    val handle: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("body_html")
    val body_html: String,
    @SerializedName("image")
    val image: Image
)  : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Image::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(handle)
        parcel.writeString(title)
        parcel.writeString(body_html)
        parcel.writeParcelable(image, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Collection> {
        override fun createFromParcel(parcel: Parcel): Collection {
            return Collection(parcel)
        }

        override fun newArray(size: Int): Array<Collection?> {
            return arrayOfNulls(size)
        }
    }
}