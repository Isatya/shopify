package com.merchant.customcollections.models

import com.google.gson.annotations.SerializedName

/**
 * Created on 20/01/19.
 */
data class GetProductsResponse(
    @SerializedName("products")
    val products: List<Product>
)