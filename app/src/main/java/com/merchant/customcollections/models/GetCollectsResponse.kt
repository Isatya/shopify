package com.merchant.customcollections.models

import com.google.gson.annotations.SerializedName
import com.merchant.customcollections.models.Collect

/**
 * Created on 20/01/19.
 */
data class GetCollectsResponse(
    @SerializedName("collects")
    val collects: List<Collect>
)