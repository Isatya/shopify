package com.merchant.customcollections.models

import com.google.gson.annotations.SerializedName

/**
 * Created on 20/01/19.
 */
data class Variant(
    @SerializedName("id")
    val id: Long,
    @SerializedName("inventory_quantity")
    val inventoryQuantity: Int
)