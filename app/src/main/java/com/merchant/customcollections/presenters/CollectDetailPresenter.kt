package com.merchant.customcollections.presenters

import com.merchant.customcollections.api.CollectionsService
import com.merchant.customcollections.models.Collect
import com.merchant.customcollections.models.Collection
import com.merchant.customcollections.models.GetCollectsResponse
import com.merchant.customcollections.models.GetProductsResponse
import com.merchant.customcollections.views.CollectionDetailView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created on 20/01/19.
 */
class CollectDetailPresenter(
    private val collectionDetailView: CollectionDetailView,
    private val collection: Collection,
    private val collectionsService: CollectionsService
) {

    fun getCollects() {
        collectionDetailView.showLoader()
        collectionsService.getCollects(collection.id, 0, "c32313df0d0ef512ca64d5b336a0d7c6")
            .enqueue(object : Callback<GetCollectsResponse> {
                override fun onResponse(call: Call<GetCollectsResponse>, response: Response<GetCollectsResponse>) {
                    if (collectionDetailView.isAlive()) {
                        if (response.isSuccessful) {
                            response.body()?.let {
                                getProducts(it.collects)
                            }
                        } else {
                            collectionDetailView.hideLoader()
                            onGetCollectsError()
                        }
                    }
                }

                override fun onFailure(call: Call<GetCollectsResponse>, t: Throwable) {
                    if (collectionDetailView.isAlive()) {
                        collectionDetailView.hideLoader()
                    }
                }
            })
    }

    fun onGetCollectsError() {}

    private fun getProducts(collects: List<Collect>) {
        var productIdsBuilder = StringBuilder()
        for (collect in collects) {
            productIdsBuilder.append(collect.productId)
            productIdsBuilder.append(",")
        }
        collectionsService.getProducts(productIdsBuilder.toString(), 0, "c32313df0d0ef512ca64d5b336a0d7c6")
            .enqueue(object : Callback<GetProductsResponse> {

                override fun onResponse(call: Call<GetProductsResponse>, response: Response<GetProductsResponse>) {
                    if (collectionDetailView.isAlive()) {
                        collectionDetailView.hideLoader()
                    }
                    if(response.isSuccessful){
                        response.body()?.let {
                            collectionDetailView.showProducts(it.products)
                        }
                    } else {
                        onGetProductsError()
                    }
                }

                override fun onFailure(call: Call<GetProductsResponse>, t: Throwable) {
                    if (collectionDetailView.isAlive()) {
                        collectionDetailView.hideLoader()
                    }
                }
            })
    }

    fun onGetProductsError(){

    }

}