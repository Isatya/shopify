package com.merchant.customcollections.presenters

import com.merchant.customcollections.api.CollectionsService
import com.merchant.customcollections.models.GetCollectionsResponse
import com.merchant.customcollections.views.CollectionsListView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created on 20/01/19.
 */
class CollectionListPresenter(
    private val collectionsListView: CollectionsListView,
    private val collectionsService: CollectionsService
) {

    fun getCollections() {
        collectionsListView.showLoader()
        collectionsService.getCustomCollectionsList(0, "c32313df0d0ef512ca64d5b336a0d7c6")
            .enqueue(object : Callback<GetCollectionsResponse> {

                override fun onResponse(
                    call: Call<GetCollectionsResponse>,
                    response: Response<GetCollectionsResponse>
                ) {
                    if (collectionsListView.isAlive()) {
                        collectionsListView.hideLoader()
                        if (response.isSuccessful) {
                            response.body()?.let {
                                collectionsListView.showCollections(it.collections)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<GetCollectionsResponse>, t: Throwable) {
                    if (collectionsListView.isAlive()) {
                        collectionsListView.hideLoader()
                    }
                }

            })
    }

}